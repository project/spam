CREATE TABLE spam_tokens (
  token varchar(255) PRIMARY KEY default '',
  spam integer NOT NULL default '0',
  notspam integer NOT NULL default '0',
  probability integer NOT NULL default '0',
  last numeric(11,0) NOT NULL default '0'
);
CREATE INDEX spam_tokens_spam_idx ON spam_tokens (spam);
CREATE INDEX spam_tokens_notspam_idx ON spam_tokens (notspam);
CREATE INDEX spam_tokens_probability_idx ON spam_tokens (probability);
CREATE INDEX spam_tokens_last_idx ON spam_tokens (last);

CREATE TABLE spam_statistics (
  name varchar(255) PRIMARY KEY default '',
  value integer NOT NULL default '0',
  last numeric(11,0) NOT NULL default '0'
);

CREATE TABLE spam_comments (
  cid integer PRIMARY KEY default '0',
  rating numeric(2,0) NOT NULL default '0',
  spam numeric(1,0) NOT NULL default '0',
  last numeric(11,0) NOT NULL default '0'
);
CREATE INDEX spam_comments_rating_idx ON spam_comments (rating);
CREATE INDEX spam_comments_spam_idx ON spam_comments (spam);
CREATE INDEX spam_comments_last_idx ON spam_comments (last);

CREATE TABLE spam_custom (
  scid serial PRIMARY KEY,
  filter varchar(255) NOT NULL default '',
  regex numeric(1,0) NOT NULL default '0',
  effect numeric(2,0) NOT NULL default '0',
  autodelete numeric(1,0) NOT NULL default '0',
  matches numeric(11,0) NOT NULL default '0',
  last numeric(11,0) NOT NULL default '0'
);
CREATE INDEX spam_custom_filter_idx ON spam_custom (filter);
CREATE INDEX spam_custom_matches_idx ON spam_custom (matches);
CREATE INDEX spam_custom_last_idx ON spam_custom (last);

CREATE TABLE spam_nodes (
  nid integer PRIMARY KEY default '0',
  rating numeric(2,0) NOT NULL default '0',
  spam numeric(1,0) NOT NULL default '0',
  hostname varchar(128) NOT NULL default '',
  last numeric(11,0) NOT NULL default '0'
);
CREATE INDEX spam_nodes_rating_idx ON spam_nodes (rating);
CREATE INDEX spam_nodes_spam_idx ON spam_nodes (spam);
CREATE INDEX spam_nodes_last_idx ON spam_nodes (last);
