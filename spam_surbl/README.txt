Overview:
--------
The spam_surbl module utilizes the multi.surbl.org Spam URI Realtime
Blocklists to detect spam.  It requires that the spam module also be
enabled.

A SURBL is a "SPAM URI Realtime BlockList". Unlike most other RBLs, 
SURBLs are not used to block spam senders. Instead they allow you to
block messages that have spam hosts which are mentioned in message 
bodies.

When new content matches an enabled SURBL, it can have one of 
three affects. If "might be spam" is selected, the new content's 
weight is slightly increased, suggesting to the spam filter this 
content might be spam. It is then up to other spam filter 
mechanisms to determine if the new content is spam. If "usually 
spam" is selected, the new content's weight is increased more 
significantly, suggesting to the spam filter that this content is
probably spam. If "always spam" is selected, the new content's 
weight is dramatically increased, telling the spam filter that 
this content is definately spam.

It is possible to enable multiple SURBL lists, in which their 
combined weights will be used. For example, if a URI matches all 6 
SURBLs, and they are all set to "might be spam", the content will 
effectively be marked as "always spam".



Features:
--------
 - written in PHP specifically for Drupal
 - supports 6 SURBLs
 - each SURBL can be individually disabled
 - each SURBL can be individually tuned so that URI matches can be 
   treated as "might be spam", "usually spam", or "always spam".


Requires:
--------
 - Drupal 4.5.x or Drupal 4.6.x
 - spam.module


Credits:
-------
 - Written by Jeremy Andrews
