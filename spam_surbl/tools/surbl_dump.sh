# two-level-tlds file obtained here:
# http://spamcheck.freeapp.net/two-level-tlds
TLDS="two-level-tlds"
OUT="spam_surbl.mysql"

cat <<SCRIPT > $OUT
DROP TABLE IF EXISTS spam_surbl_cctlds;
CREATE TABLE spam_surbl_cctlds (
  ccid int(4) unsigned NOT NULL auto_increment,
  cc varchar(255) NOT NULL default '',
  PRIMARY KEY ccid (ccid),
  UNIQUE KEY cc (cc)
) TYPE=MyISAM;

SCRIPT

I=1;
for TLD in `cat $TLDS`
do
  echo "INSERT INTO spam_surbl_cctlds VALUES(\"$I\",\"$TLD\");" >> $OUT
  I=`expr $I + 1`
done
