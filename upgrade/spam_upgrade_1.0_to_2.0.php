<?php
include_once 'includes/bootstrap.inc';
drupal_page_header();
include_once 'includes/common.inc';

/* delete the spam comments */
$count = 0;
$result = db_query('SELECT cid FROM {spam_comments} WHERE spam = 1');
while ($c = db_fetch_object($result)) {
  if ($c->cid != 0) {
    $comment = spam_load_comment($c->cid);
    if ($comment && $comment->cid != 0) {
      _comment_delete_thread($comment);
      _comment_update_node_statistics($comment->nid);
      $count++;
    }
  }
}
echo "Found and deleted $count spam comments.<br />";

/* delete the spam nodes */
$count = 0;
$result = db_query('SELECT nid FROM {spam_nodes} WHERE spam = 1');
while ($n = db_fetch_object($result)) {
  if ($n->nid != 0) {
    $node = node_load(array('nid' => $n->nid));
    if ($node && $node->nid != 0) {
      $node->confirm = 1;
      node_delete(object2array($node));
      $count++;
    }
  }
}
echo "Found and deleted $count spam nodes.<br /><br />";

cache_clear_all();

/* The above logic may time out in the browser and have to be run multiple 
 * times.  The following logic can only be run once.
 */
if (variable_get('spam_2.0_upgrade_run', 0)) {
  echo "You have already run this script, you can't run it again.<br />";
  exit (1);
}
variable_set('spam_2.0_upgrade_run', time());

db_query('DROP TABLE {spam_comments}');
echo "Removed the {spam_comments} table.<br />";
db_query('DROP TABLE {spam_nodes}');
echo "Removed the {spam_nodes} table.<br />";

// drop tables we no longer use
db_query('DROP TABLE {spam_statistics}');
echo "Removed the {spam_statistics} table.<br />";

// intentionally drop and recreate this table for a fresh start
db_query('DROP TABLE {spam_tokens}');
echo "Removed the {spam_tokens} table.<br /><br />";


// create new tables
db_query("CREATE TABLE {spam_tracker} (
  sid int(11) unsigned NOT NULL auto_increment,
  source varchar(64) NOT NULL default '',
  id int(11) unsigned NOT NULL default '0',
  probability int(2) unsigned default '0',
  hostname varchar(128) NOT NULL default '',
  hash char(32) NOT NULL default '',
  timestamp int(11) unsigned default '0',
  PRIMARY KEY sid (sid),
  KEY id (id),
  KEY probability (probability),
  KEY source (source),
  KEY hostname (hostname),
  KEY hash (hash)
) TYPE=MyISAM;");
echo "Created the new {spam_tracker} table.<br />";

db_query("CREATE TABLE {spam_tokens} (
  tid int(10) unsigned NOT NULL auto_increment,
  token varchar(255) NOT NULL default '',
  spam int(10) unsigned default '0',
  notspam int(10) unsigned default '0',
  probability int(10) unsigned default '0',
  last int(11) unsigned default '0',
  PRIMARY KEY tid (tid),
  UNIQUE KEY token (token),
  KEY spam (spam),
  KEY notspam (notspam),
  KEY probability (probability),
  KEY last (last)
) TYPE=MyISAM;");
echo "Created the new {spam_tokens} table.<br />";

db_query("CREATE TABLE {spam_log} (
  sid int(11) unsigned NOT NULL auto_increment,
  source varchar(64) NOT NULL default '',
  id int(11) unsigned NOT NULL default '0',
  uid int(10) unsigned NOT NULL default '0',
  hostname varchar(128) NOT NULL default '',
  entry varchar(255) NOT NULL default '',
  timestamp int(11) unsigned default '0',
  PRIMARY KEY sid (sid),
  KEY source (source),
  KEY id (id),
  KEY timestamp (timestamp)
) TYPE=MyISAM;");
echo "Created the new {spam_log} table.<br /><br />";

// update the custom filters table
db_query("ALTER TABLE {spam_custom} CHANGE regex style int(2) unsigned default '0'");
db_query("ALTER TABLE {spam_custom} CHANGE autodelete action int(2) unsigned default '0'");
echo "Updated the {spam_custom} table.<br /><br />";

db_query("DELETE FROM {cache}");
echo "Emptied the {cache} table.<br /><br />";

echo "Now you must replace your old spam.module with the new 2.0 spam.module.<br />";
echo "If you run into any problems, try manually emptying the cache table by executing the following command in mysql:<br />";
echo "<pre>        DELETE FROM cache;</pre><br /><br />";

echo "You may safely remove this 'spam_upgrade.php' script.<br />";
echo "Please read the INSTALL.txt file that came with the 2.0 spam module for more configuration and usage information.<br />";

drupal_page_footer();

?>
