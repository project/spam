Optional enhancements to the spam module.

To learn more about these optional patches, open them in a text editor
and review the comments at the beginning.
